package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ForgetPasswordScreen extends BaseTest {

    //region FindBy
    @FindBy(how = How.ID, using = "pl.tablica:id/loginBtn")
    public WebElement btn_loginViaMail;

    @FindBy(how = How.ID, using = "pl.tablica:id/txtForgetPassword")
    public WebElement btn_txtForgetPassword;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.tablica:id/doneButton")
    public WebElement btn_doneButton;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnConfirm")
    public WebElement btn_Confirm;

    @FindBy(how = How.XPATH, using = "//*[containts(@text, 'Kod weryfikacyjny')]")
    public WebElement input_Kodweryfikacyjny;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement btn_inputNewPassword;

    @FindBy(how = How.ID, using = "pl.tablica:id/doneButton")
    public WebElement btn_doneButton2;
    //endregion FindBy

//    public void forgetPassword() {
//        btn_loginViaMail.click();
//        btn_txtForgetPassword.click();
//        waitTime(1);
//        input_email.sendKeys("saberhagen93@gmail.com");
//        btn_doneButton.click();
//        btn_Confirm.click();
//
//        goHome();
//        waitTime(2);
//        tapCoordinates(336, 2089);
//        waitTime(4);
//        tapCoordinates(352, 469);
//        waitTime(5);
//        tapCoordinates(545, 524);
//
//        longPress(540, 1538);
//        waitTime(3);
//        tapCoordinates(535, 1544);
//        waitTime(2);
//        goBack();
//        waitTime(4);
//        goBack();
//        tapCoordinates(963, 519);
//        waitTime(1);
//        tapCoordinates(963, 519);
//
//        btn_inputNewPassword.sendKeys("NoweHaslo12345");
//        btn_doneButton2.click();
//
//    }
}
