package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.sql.SQLOutput;


public class LoginScreen extends BaseTest {

    //region FindBy
    @FindBy(how = How.ID, using = "pl.tablica:id/loginBtn")
    public WebElement btn_loginViaMail;

    @FindBy(how = How.ID, using = "pl.tablica:id/tabLogin")
    public WebElement btn_tabLogin;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_logInNew;

    @FindBy(how = How.ID, using = "pl.tablica:id/actionButton")
    public WebElement btn_actionButton;

    @FindBy(how = How.ID, using = "pl.tablica:id/closeButton")
    public WebElement btn_closeButton;

    //endregion FindBy

    MethodHelper helper = new MethodHelper();

    public void loginToApp(String name, String password) {


        btn_loginViaMail.click();
        btn_tabLogin.click();

        input_email.sendKeys(name);
        input_password.sendKeys(password);
        btn_logInNew.click();

    }

    public void loginToAppTwo() {

        helper.waitTime(8);

        helper.swipeInDirection(MethodHelper.direction.UP, "up", 0.8);

        btn_actionButton.click();

        helper.waitTime(2);

        helper.tapCoordinates(790, 1960);

    }
}
