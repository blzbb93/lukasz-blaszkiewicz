package com.selenium.java.base.pages.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class SearchScreen extends BaseTest {

    //region FindBy
    @FindBy(how = How.ID, using = "")
    public WebElement btn_;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, '')]")
    public WebElement input_;
    ///
    @FindBy(how = How.ID, using = "pl.tablica:id/searchInput")
    public WebElement btn_searchInput;

    @FindBy(how = How.ID, using = "pl.tablica:id/searchInput")
    public WebElement btn_searchBar;

    @FindBy(how = How.ID, using = "pl.tablica:id/sortButton")
    public WebElement btn_sortButton;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView[2]\n")
    public WebElement btn_theCheapest;

    @FindBy(how = How.XPATH, using = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.ImageView[1]\n")
    public WebElement btn_firstAd;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnChat")
    public WebElement btn_Chat;

    @FindBy(how = How.ID, using = "pl.tablica:id/caption")
    public WebElement btn_caption;

    @FindBy(how = How.ID, using = "pl.tablica:id/category_suggestion_text_1")
    public WebElement btn_suggestion;
//endregion FindBy

    MethodHelper helper = new MethodHelper();

    public void searchScreenGo(String adtitle) {

        helper.waitTime(2);
        btn_searchInput.click();
        helper.waitTime(2);
        btn_searchBar.sendKeys(adtitle);
        helper.waitTime(1);
        helper.goEnter();
        helper.waitTime(2);
        helper.tapCoordinates(183, 453);
        helper.waitTime(4);
        helper.tapCoordinates(958, 515);
        helper.waitTime(2);
        btn_sortButton.click();
        btn_theCheapest.click();
        btn_firstAd.click();

    }
}
