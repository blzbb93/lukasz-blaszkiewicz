package com.selenium.java.base.pages.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class AddAdPage extends BaseTest {

    MethodHelper helper = new MethodHelper();
    Robot robot = new Robot();

    @FindBy(how = How.CLASS_NAME, using = "button")
    public WebElement btn_button;

    @FindBy(how = How.ID, using = "add-title")
    public WebElement input_tytul;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetrenderSelect1-0\"]/dt")
    public WebElement input_category;

    @FindBy(how = How.ID, using = "cat-584")
    public WebElement btn_domOgrod;

    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/div/div[1]/input")
    public WebElement input_cena;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetparam13\"]/li[2]/a/span[1]")
    public WebElement btn_nowe;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetid_private_business\"]/li[2]/a/span[1]")
    public WebElement btn_osobaPrywatna;

    @FindBy(how = How.ID, using = "add-description")
    public WebElement input_addDescription;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-file-1\"]/div/a")
    public WebElement btn_addPhoto;

    @FindBy(how = How.ID, using = "mapAddress")
    public WebElement input_localization;

    @FindBy(how = How.XPATH, using = "//*[@id=\"map_address\"]/div[2]/div[1]/div/div/div[1]")
    public WebElement btn_confirm;

    @FindBy(how = How.ID, using = "add-person")
    public WebElement input_addPerson;

    @FindBy(how = How.ID, using = "add-phone")
    public WebElement input_phone;

    @FindBy(how = How.XPATH, using = "//*[@id=\"newOffer\"]/div/div[2]/div[2]/div/div")
    public WebElement btn_checkbox;

    @FindBy(how = How.ID, using = "preview-link")
    public WebElement btn_checkAd;

    public AddAdPage() throws AWTException {
    }

    public void addAd(String tytul, String cena, String description, String user, String photolink) throws AWTException {

        btn_button.click();
        input_tytul.sendKeys(tytul);
        input_category.click();
        helper.waitTime(1);
        btn_domOgrod.click();
        input_cena.sendKeys(cena);
        btn_nowe.click();
        btn_osobaPrywatna.click();
        input_addDescription.sendKeys(description);
        btn_addPhoto.click();
        helper.waitTime(2);

        StringSelection ss = new StringSelection(photolink);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        input_localization.sendKeys("87-100");
        btn_confirm.click();
        input_addPerson.sendKeys(user);
        input_phone.sendKeys("737467555");
        btn_checkbox.click();
        helper.waitTime(3);
        btn_checkAd.click();

    }
}

