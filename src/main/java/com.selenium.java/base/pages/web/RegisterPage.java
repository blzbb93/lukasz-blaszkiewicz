package com.selenium.java.base.pages.web;

import com.selenium.java.base.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class RegisterPage extends BaseTest {

    @FindBy(how = How.ID, using = "my-account-link")
    public WebElement input_myAccont;

    @FindBy(how = How.ID, using = "register_tab")
    public WebElement btn_registerTab;

    @FindBy(how = How.ID, using = "userEmailRegister")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "userPassRegister")
    public WebElement input_userPassRegister;

    @FindBy(how = How.CLASS_NAME, using = "cookiesBarClose")
    public WebElement btn_closeCookies;

    @FindBy(how = How.XPATH, using = "//*[@id=\"registerForm\"]/div[4]/div/div/label[1]")
    public WebElement btn_checkBox;

    @FindBy(how = How.ID, using = "button_register")
    public WebElement btn_register;


    public void register(String name, String pass) {

        input_myAccont.click();
        btn_registerTab.click();
        input_email.sendKeys(name);
        input_userPassRegister.sendKeys(pass);
        btn_closeCookies.click();
        btn_checkBox.click();
        btn_register.click();


    }
}
