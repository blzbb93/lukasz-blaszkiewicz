package com.selenium.java.base.pages.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchPage extends BaseTest {

    MethodHelper helper = new MethodHelper();

    @FindBy(how = How.ID, using = "headerLogo")
    public WebElement btn_logo;

    @FindBy(how = How.ID, using = "headerSearch")
    public WebElement btn_search;

    @FindBy(how = How.ID, using = "submit-searchmain")
    public WebElement btn_submitSearchmain;

    @FindBy(how = How.XPATH, using = "//*[@id=\"paramsListOpt\"]/div/div[2]/label[1]")
    public WebElement btn_photoOnly;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]")
    public WebElement btn_sort;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetorder-select-gallery\"]/dd/ul/li[2]\n")
    public WebElement btn_sortCheapest;

    @FindBy(how = How.XPATH, using = "//*[@id=\"body-container\"]/div[3]/div/div[1]/table[1]/tbody/tr[3]/td/div/table/tbody/tr[1]/td[2]/div/h3/a")
    public WebElement btn_clickAd;

    @FindBy(how = How.XPATH, using = "//*[@id=\"contact_methods\"]/li[1]/a")
    public WebElement btn_call;


    public void search(String title) {

        btn_logo.click();
        btn_search.sendKeys(title);
        helper.waitTime(2);
        btn_submitSearchmain.click();
        helper.waitTime(2);
        btn_photoOnly.click();
        helper.waitTime(4);
        btn_sort.click();
        btn_sortCheapest.click();
        helper.waitTime(2);
        btn_clickAd.click();
        helper.waitTime(3);
        btn_call.click();
        helper.waitTime(5);


    }
}
