package com.selenium.java.tests.presentation.automaty;

import org.testng.annotations.*;

public class DataProviderTest {

    @BeforeClass
    public void startup() {
        System.out.println("Before Class");
    }

    @BeforeMethod
    public void setUp() {
        System.out.println("Before Method");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
        {5, "five", 4},
        {6, "six", 5},
        {7, "seven", 7},
        {8, "eight", 4}
    };
    }

    @Test(dataProvider = "getData")
    public void lobby(int p1, String p2, int p3) {

        System.out.println("Test");
        System.out.println(p1 + " " + p2 + " " + p3);
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("After Method");
    }

        @AfterClass
        public void stop () {
            System.out.println("AfterClass");
        }
    }
