package com.selenium.java.tests.presentation.automaty;

import org.testng.annotations.*;

public class TestStructure {


    @BeforeClass
    public void startup() {
        System.out.println("I'm in BeforeClass");
    }

    @BeforeMethod
    public void setUp() {

        System.out.println("I'm in BeforeMethod");
    }

    @Test()
    public void test1() {

        System.out.println("I'm in Test1");
    }

    @Test()
    public void test2() {

        System.out.println("I'm in Test2");
    }

    @Test()
    public void test3() {

        System.out.println("I'm in Test3");
    }

    @Test()
    public void test4() {

        System.out.println("I'm in Test4");
    }

    @AfterMethod
    public void tearDown() {

        System.out.println("I'm in AfterMethod");
    }

    @AfterClass
    public void stop() {

        System.out.println("I'm in AfterClass");
    }
}

