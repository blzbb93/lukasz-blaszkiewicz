package com.selenium.java.tests.presentation.examples;

import java.util.Arrays;

public class ImionaAlfabetycznie {

    public enum Names {
        ZOSIA,
        MAREK,
        PIOTR,
        ANIA,
        CZAREK,
        DAWID,
        MARTA,
        MAGDA,
        EMIL,
        DAMIAN
    }

    public static void main(String[] args) {
        String tab[] = new String[10];

        ChooseName(Names.ZOSIA, tab);
        ChooseName(Names.MAREK, tab);
        ChooseName(Names.PIOTR, tab);
        ChooseName(Names.ANIA, tab);
        ChooseName(Names.CZAREK, tab);
        ChooseName(Names.DAWID, tab);
        ChooseName(Names.MARTA, tab);
        ChooseName(Names.MAGDA, tab);
        ChooseName(Names.EMIL, tab);
        ChooseName(Names.DAMIAN, tab);

        System.out.println("Rozmiar tablicy wynosi: " + tab.length);
        System.out.println();


        System.out.println("Lista nieposortowana");
        for (int i = 0; i < tab.length; i++) {
            System.out.println(tab[i]);
        }

        System.out.println();
        System.out.println("Lista posortowana:");

        Arrays.sort(tab);
        for (int i = 0; i < tab.length; i++) {
            System.out.println(tab[i]);
        }
    }


    public static void ChooseName(Names imie, String tab[]) {

        switch (imie) {

            case ZOSIA:
                tab[0] = imie.name();
                break;
            case MAREK:
                tab[1] = imie.name();
                break;
            case PIOTR:
                tab[2] = imie.name();
                break;
            case ANIA:
                tab[3] = imie.name();
                break;
            case CZAREK:
                tab[4] = imie.name();
                break;
            case DAWID:
                tab[5] = imie.name();
                break;
            case MARTA:
                tab[6] = imie.name();
                break;
            case MAGDA:
                tab[7] = imie.name();
                break;
            case EMIL:
                tab[8] = imie.name();
                break;
            case DAMIAN:
                tab[9] = imie.name();
                break;
        }
    }
}







