package com.selenium.java.tests.presentation.examples;

public class Person {

    String name;
    int age;
    boolean isAlive;

    void przedstawSie() {

        String welcome;

        if (isAlive) {
            welcome = "i nadal żyję";
        } else {
            welcome = "i już mnie nie ma";
        }

        System.out.println("Cześć, jestem " + name + " mam " + age + " lat " + welcome);
    }

    void przedstawSie(String name, int age, boolean isAlive) {
        String aaa;
        if (isAlive) {
            aaa = "i nadal żyję";
        } else {
            aaa = "i już mnie nie ma";
        }
        System.out.println("Cześć jestem " + name + ", mam " + age + " lat " + aaa);
    }
}
