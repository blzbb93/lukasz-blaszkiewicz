package com.selenium.java.tests.presentation.examples;

public class Projekt1 {

    public double obliczSrednia(double... Oceny) {

        double suma = 0;

        for (double ocena : Oceny) {
            suma = suma + ocena;
        }
        return suma / Oceny.length;
    }
}
