package com.selenium.java.tests.presentation.presentation;

public class CommentsTypes {
    /**
     There are 3 types of comments in java
     */
    //region Single Line Comment

    // The single line comment is used to comment only 1 line

    //endregion Single Line Comment

    //region Multi Line Comment

    /*
        The multi line comment
        is used to comment
        multiple lines of code.
    */

    //endregion Multi Line Comment
    //region Documentation Comment

    /**
     The documentation comment is used to
     create documentation API.
     To create documentation API,
     you need to use javadoc tool.
     */
    //endregion Documentation Comment
}