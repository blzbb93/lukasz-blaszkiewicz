package com.selenium.java.tests.presentation.presentation;

public class MethodOverLoading {

    public static void main(String[] args) {

        method("Karolina", 12);
        method("Ewa");
        method();
    }


    public static void method(String name, int age) {

        System.out.println(name + " ma " + age + " lat");
    }

    public static void method(String name) {
        System.out.println(name + " ma 65 lat");
    }

    public static void method() {
        System.out.println("Ala ma 35 lat");
    }
}

