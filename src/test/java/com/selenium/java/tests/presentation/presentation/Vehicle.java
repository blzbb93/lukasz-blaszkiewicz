package com.selenium.java.tests.presentation.presentation;

public class Vehicle {

    public enum Colors {
        BLUE,
        RED,
        YELLOW,
        PINK
    }

    public static void startEngine() {
        System.out.println("Engine starts. Brum brum.");
    }

    public static void myCar(Colors colors) {
        System.out.println("My car is " + colors);
    }

    public static void stopEngine() {
        System.out.println("Stop Engine.");
    }
}
