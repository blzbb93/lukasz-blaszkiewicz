package com.selenium.java.tests.presentation.test.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.CreateAccountScreen;
import com.selenium.java.base.pages.android.LoginScreen;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.lang.reflect.Method;


public class CreateAccountTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceID, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceID, deviceName, context);

    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"lukasz_tester_a11@wp.pl", "Tester123##"},
                {"lukasz_tester_a12@wp.pl", "Tester123##"},
                {"lukasz_tester_a13@wp.pl", "Tester123##"},
                {"lukasz_tester_a14@wp.pl", "Tester123##"},
                {"lukasz_tester_a15@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getLoginOnly() {
        return new Object[][]{
                {"lukasz_tester_1x1@wp.pl", ""},
                {"lukasz_tester_2x2wp.pl", ""},
                {"lukasz_tester_3x3@wp.pl", ""},
                {"lukasz_tester_4x4@wp.pl", ""},
                {"lukasz_tester_5x5@wp.pl", ""},
        };
    }

    @DataProvider
    public Object[][] getPasswordOnly() {
        return new Object[][]{
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongMail() {
        return new Object[][]{
                {"lukasz_tester_1@w@p.pl", "Tester123##"},
                {"lukasz_tester_1@wpp", "Tester123##"},
                {"lukasz_tester_1@", "Tester123##"},
                {"lukasz_tester_1", "Tester123##"},
                {"@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{
                {"lukasz_tester_1q1x@wp.pl", "tester123##"},
                {"lukasz_tester_2q2x@wp.pl", "tester"},
                {"lukasz_tester_3q3x@wp.pl", "tester123"},
                {"lukasz_tester_4q4x@wp.pl", "Tester"},
                {"lukasz_tester_5q5x@wp.pl", "lukasz_tester_5@wp.pl"},
        };
    }

    MethodHelper helper = new MethodHelper();

    @Test(dataProvider = "getData")
    public void testCreateAccount(String name, String password) {

        WebDriver driver = getDriver();
        CreateAccountScreen createAccountScreen = PageFactory.initElements(driver, CreateAccountScreen.class);
        createAccountScreen.createAccount(name, password);

        helper.waitTime(1);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnBrowse")).isDisplayed());

        helper.getScreenShot("Create account test.png");
    }

    @Test(dataProvider = "getLoginOnly")
    public void testCreateAccountWithoutPassword(String name, String password) {

        WebDriver driver = getDriver();
        CreateAccountScreen createAccountScreen = PageFactory.initElements(driver, CreateAccountScreen.class);
        createAccountScreen.createAccount(name, password);

        helper.waitTime(1);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error")).isDisplayed());

        helper.getScreenShot("Create account without password.png");
    }

    @Test(dataProvider = "getPasswordOnly")
    public void testCreateAccountWithoutLogin(String name, String password) {

        WebDriver driver = getDriver();
        CreateAccountScreen createAccountScreen = PageFactory.initElements(driver, CreateAccountScreen.class);
        createAccountScreen.createAccount(name, password);

        helper.waitTime(1);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error")).isDisplayed());

        helper.getScreenShot("Create account without password.png");
    }

    @Test(dataProvider = "getWrongMail")
    public void testCreateAccountWithWrongMail(String name, String password) {

        WebDriver driver = getDriver();
        CreateAccountScreen createAccountScreen = PageFactory.initElements(driver, CreateAccountScreen.class);
        createAccountScreen.createAccount(name, password);
        helper.waitTime(1);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error")).isDisplayed());

        helper.getScreenShot("Create account with wrong mail.png");
    }

    @Test(dataProvider = "getWrongPassword")
    public void testCreateAccountWithWrongPassword(String name, String password) {

        WebDriver driver = getDriver();
        CreateAccountScreen createAccountScreen = PageFactory.initElements(driver, CreateAccountScreen.class);
        createAccountScreen.createAccount(name, password);

        helper.waitTime(1);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error")).isDisplayed());

        helper.getScreenShot("Create account with wrong password.png");
    }


    @AfterMethod
    public void tearDown() {

        System.out.println((char) 27 + "[35m" + "Koniec testów zakładania konta");
    }
}



