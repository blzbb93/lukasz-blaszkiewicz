package com.selenium.java.tests.presentation.test.android;

import com.google.common.collect.ImmutableMap;
import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.LoginScreen;
import io.appium.java_client.functions.ExpectedCondition;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.lang.reflect.Method;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;


public class LoginTest extends BaseTest {

    @BeforeClass
    public void startup() {


        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceID, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceID, deviceName, context);

    }


    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"lukasz_tester_1@wp.pl", "Tester123##"},
                {"lukasz_tester_2@wp.pl", "Tester123##"},
                {"lukasz_tester_333@wp.pl", "Tester123##"},
                {"lukasz_tester_444@wp.pl", "Tester123##"},
                {"lukasz_tester_555@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getLoginOnly() {
        return new Object[][]{
                {"lukasz_tester_1@wp.pl", ""},
                {"lukasz_tester_2@wp.pl", ""},
                {"lukasz_tester_333@wp.pl", ""},
                {"lukasz_tester_444@wp.pl", ""},
                {"lukasz_tester_555@wp.pl", ""},
        };
    }

    @DataProvider
    public Object[][] getPasswordOnly() {
        return new Object[][]{
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongMail() {
        return new Object[][]{
                {"lukasz_tester_1@@wpp.pl", "Tester123##"},
                {"lukasz_tester_1@wpp", "Tester123##"},
                {"lukasz_tester_1@", "Tester123##"},
                {"lukasz_tester_1", "Tester123##"},
                {"@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{
                {"lukasz_tester_1@wp.pl", "tester123##"},
                {"lukasz_tester_2@wp.pl", "Tester123#"},
                {"lukasz_tester_333@wp.pl", "tester123"},
                {"lukasz_tester_444@wp.pl", "Tester"},
                {"lukasz_tester_555@wp.pl", "lukasz_tester_5@wp.pl"},
        };
    }

    MethodHelper helper = new MethodHelper();


    @Test(dataProvider = "getData", description = "Checking basic functionality")
    public void testLoginToApp(String name, String password) {

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        helper.waitTime(3);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/closeButton")).isDisplayed());

        helper.getScreenShot("Login to app.png");
    }


    @Test(dataProvider = "getLoginOnly", description = "Checking basic functionality")
    public void testLoginWithoutPassword(String name, String password) {

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error")).isDisplayed());

        helper.getScreenShot("Login to app without password.png");
    }


    @Test(dataProvider = "getPasswordOnly", description = "Checking basic functionality")
    public void testLoginWithoutName(String name, String password) {

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error")).isDisplayed());

        helper.getScreenShot("Login to app without name.png");
    }

    @Test(dataProvider = "getWrongMail", description = "Checking basic functionality")
    public void testLoginWithWrongMail(String name, String password) {

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/textinput_error")).isDisplayed());

        helper.getScreenShot("Login to app with wrong mail.png");
    }


    @Test(dataProvider = "getWrongPassword", description = "Checking basic functionality")
    public void testLoginWithWrongPassword(String name, String password) {

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnLogInNew")).isDisplayed());

        helper.getScreenShot("Login to app with wrong password.png");
    }


    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        System.out.println((char) 27 + "[35m" + "Koniec testów logowania");
    }
}




