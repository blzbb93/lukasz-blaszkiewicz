package com.selenium.java.tests.presentation.test.android;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.LoginScreen;
import com.selenium.java.base.pages.android.SearchScreen;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;



public class SearchTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceID, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceID, deviceName, context);

    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Rower górski"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Rower miejski"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Laptop Dell"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Monitor LG"},
                {"celestiallucifer@gmail.com", "Xxxxxx11pkdw", "Telewizor Sony"}
        };
    }

    MethodHelper helper = new MethodHelper();
    @Test(dataProvider = "getData")
    public void testSearch(String name, String password, String adtitle) {

        WebDriver driver = getDriver();
        SearchScreen searchScreen = PageFactory.initElements(driver, SearchScreen.class);
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(name, password);
        loginScreen.loginToAppTwo();
        searchScreen.searchScreenGo(adtitle);

        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnChat")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.id("pl.tablica:id/btnPhone")).isDisplayed());

        helper.getScreenShot("Test filtrowania.png");
    }

    @AfterMethod
    public void tearDown() {

        System.out.println((char) 27 + "[35m" + "Koniec testów logowania");
    }
}



