package com.selenium.java.tests.presentation.test.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.CreateAccountScreen;
import com.selenium.java.base.pages.android.LoginScreen;
import com.selenium.java.base.pages.web.AddAdPage;
import com.selenium.java.base.pages.web.LoginPage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.awt.*;

public class AddAdWebTest extends BaseTest {

    MethodHelper helper = new MethodHelper();

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false; //uncomment if u want a headless mode
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"Świerk", "50", "Roślina ogrodowa, szybko rosnąca tudzież w ogóle", "Nasturcja Lover", "C:\\Users\\Programista\\Desktop\\Test Photos\\swierk.png"},
//                {"Basen", "150", "Basen ogrodowy, nieprzeciekąjacy", "Marek Makowski", "C:\\Users\\Programista\\Desktop\\Test Photos\\basen.png"},
//                {"Paprotka", "5", "Rosnaca nieprzerwanie od czasu PRL", "UB General", "C:\\Users\\Programista\\Desktop\\Test Photos\\paprotka.png"},
//                {"Róże ogrodowe", "8", "Likwidacja kwiaciarni - tylko priv", "Mariola Kwiatkowska", "C:\\Users\\Programista\\Desktop\\Test Photos\\roze.png"},
//                {"Kaktus", "20", "Latwy w utrzymaniu kwiat; polecany gdy studnia znajduje się 3 kilometry od miejsca zamieszkania", "Africa Dreamer", "C:\\Users\\Programista\\Desktop\\Test Photos\\kaktus1.png"}
        };
    }


    @Test(dataProvider = "getData", priority = 1, description = "Adding advertisement test")
    @Severity(SeverityLevel.NORMAL)
    @Step("Go to testAddAd")
    @Story("Adding advertisement TEST")

    public void testAddAd(String tytul, String cena, String description, String user, String photolink) throws AWTException {

        WebDriver driver = getDriver();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        AddAdPage addAdPage = PageFactory.initElements(driver, AddAdPage.class);
        loginPage.loginToAppWithData();
        addAdPage.addAd(tytul, cena, description, user, photolink);

        Assert.assertTrue(driver.findElement(By.id("save-from-preview")).isDisplayed());

        helper.getScreenShot("Adding advertisement test.png");
        helper.getScreenshotForAllure("xaxa");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
        driver.close();
    }
}
