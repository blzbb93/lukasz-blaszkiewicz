package com.selenium.java.tests.presentation.test.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.web.AddAdPage;
import com.selenium.java.base.pages.web.Categories;
import com.selenium.java.base.pages.web.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;


public class CategoriesWebTest extends BaseTest {

    MethodHelper helper = new MethodHelper();

    @BeforeMethod
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false; //uncomment if u want a headless mode
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @Test
    public void testCategories() {

        WebDriver driver = getDriver();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Categories categories = PageFactory.initElements(driver, Categories.class);
        loginPage.loginToAppWithData();

    }

    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
        driver.close();
    }
}
