package com.selenium.java.tests.presentation.test.web;


import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.android.CreateAccountScreen;
import com.selenium.java.base.pages.android.LoginScreen;
import com.selenium.java.base.pages.web.LoginPage;
import com.selenium.java.base.pages.web.RegisterPage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class RegisterWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = true; //uncomment if u want a headless mode
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"lukasz_tester_a11h@wp.pl", "Tester123##"},
                {"lukasz_tester_a12h@wp.pl", "Tester123##"},
                {"lukasz_tester_a13h@wp.pl", "Tester123##"},
                {"lukasz_tester_a14h@wp.pl", "Tester123##"},
                {"lukasz_tester_a15h@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getLoginOnly() {
        return new Object[][]{
                {"lukasz_tester_1x1@wp.pl", ""},
                {"lukasz_tester_2x2wp.pl", ""},
                {"lukasz_tester_3x3@wp.pl", ""},
                {"lukasz_tester_4x4@wp.pl", ""},
                {"lukasz_tester_5x5@wp.pl", ""},
        };
    }

    @DataProvider
    public Object[][] getPasswordOnly() {
        return new Object[][]{
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
                {"", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongMail() {
        return new Object[][]{
                {"lukasz_tester_1@w@p.pl", "Tester123##"},
                {"lukasz_tester_1@wpp", "Tester123##"},
                {"lukasz_tester_1@", "Tester123##"},
                {"lukasz_tester_1", "Tester123##"},
                {"@wp.pl", "Tester123##"},
        };
    }

    @DataProvider
    public Object[][] getWrongPassword() {
        return new Object[][]{
                {"lukasz_tester_1q1x@wp.pl", "tester123##"},
                {"lukasz_tester_2q2x@wp.pl", "tester"},
                {"lukasz_tester_3q3x@wp.pl", "tester123"},
                {"lukasz_tester_4q4x@wp.pl", "Tester"},
                {"lukasz_tester_5q5x@wp.pl", "lukasz_tester_5@wp.pl"},
        };
    }

    MethodHelper helper = new MethodHelper();

    @Test(dataProvider = "getData", priority = 1, description = "Registration test with correct username and correct password")
    @Severity(SeverityLevel.CRITICAL)
    @Step("Go to testCreateAccount")
    @Story("Correct username and correct password TEST")

    public void testCreateAccount(String name, String pass) {

        WebDriver driver = getDriver();
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.register(name, pass);
        Assert.assertTrue(driver.findElement(By.className("login-box-confirm")).isDisplayed());

        helper.getScreenShot("Registration test with correct username and correct password.png");
    }

    @Test(dataProvider = "getLoginOnly", priority = 2, description = "Registration test without password")
    @Severity(SeverityLevel.MINOR)
    @Step("Go to testCreateAccountWithoutPassword")
    @Story("Correct username and invalid password TEST")

    public void testCreateAccountWithoutPassword(String name, String pass) {

        WebDriver driver = getDriver();
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.register(name, pass);
        Assert.assertTrue(driver.findElement(By.id("button_register")).isDisplayed());

        helper.getScreenShot("Registration test without password");
    }

    @Test(dataProvider = "getPasswordOnly", priority = 2, description = "Registration test without username")
    @Severity(SeverityLevel.MINOR)
    @Step("Go to testCreateAccountWithoutLogin")
    @Story("Invalid username and correct password TEST")

    public void testCreateAccountWithoutLogin(String name, String pass) {

        WebDriver driver = getDriver();
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.register(name, pass);
        Assert.assertTrue(driver.findElement(By.id("button_register")).isDisplayed());

        helper.getScreenShot("Registration test without username.png");
    }

    @Test(dataProvider = "getWrongMail", priority = 2, description = "Registration test with wrong e-mail")
    @Severity(SeverityLevel.MINOR)
    @Step("Go to testCreateAccountWithWrongMail")
    @Story("Invalid username and correct password TEST")

    public void testCreateAccountWithWrongMail(String name, String pass) {

        WebDriver driver = getDriver();
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.register(name, pass);
        Assert.assertTrue(driver.findElement(By.id("button_register")).isDisplayed());

        helper.getScreenShot("Registration test with wrong e-mail.png");
    }

    @Test(dataProvider = "getWrongPassword", priority = 2, description = "Registration test with wrong password")
    @Severity(SeverityLevel.MINOR)
    @Step("Go to testCreateAccountWithWrongPassword")
    @Story("Correct username and invalid password TEST")

    public void testCreateAccountWithWrongPassword(String name, String pass) {

        WebDriver driver = getDriver();
        RegisterPage registerPage = PageFactory.initElements(driver, RegisterPage.class);
        registerPage.register(name, pass);
        Assert.assertTrue(driver.findElement(By.id("button_register")).isDisplayed());

        helper.getScreenShot("Registration test with wrong password.png");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        System.out.println((char) 27 + "[35m" + "Koniec testów zakładania konta");
    }
}



