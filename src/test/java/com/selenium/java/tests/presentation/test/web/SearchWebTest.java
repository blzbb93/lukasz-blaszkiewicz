package com.selenium.java.tests.presentation.test.web;

import com.selenium.java.base.base.BaseTest;
import com.selenium.java.base.helper.MethodHelper;
import com.selenium.java.base.pages.web.LoginPage;
import com.selenium.java.base.pages.web.SearchPage;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;


public class SearchWebTest extends BaseTest {

    MethodHelper helper = new MethodHelper();

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false; //uncomment if u want a headless mode
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"Komoda"},
                {"Rower górski"},
                {"Szafa"},
        };
    }

    @Test(dataProvider = "getData", priority = 3, description = "Searching test")
    @Severity(SeverityLevel.NORMAL)
    @Step("Go to searchPage")
    @Story("Searching TEST")

    public void searchPage(String title) {

        WebDriver driver = getDriver();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        loginPage.loginToAppWithData();
        searchPage.search(title);
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"contact_methods\"]/li[1]/a")).isDisplayed());
        helper.getScreenShot("Search test.png");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        WebDriver driver = getDriver();
        System.out.println("Test ended");
        driver.close();
    }
}
